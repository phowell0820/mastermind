﻿using UnityEngine;
using System;
using System.Collections;

public class GameManager : MonoBehaviour {

	// # of different colored balls available to the player
	public int balls;

	// # of different balls in the solution
	public int complexity;

	// # of tries the player has to get the right solution
	public int tries;

	// If colors can repeat in the solution or not
	public bool repeat;

	// Plane prefab which acts as the players game area where balls are contained
	public GameObject PlayerArea;
	public GameObject CodeArea;
	public GameObject SubmitArea;
	public GameObject MainCamera;

	// Generic UI prefabs used for UI creation
	public GameObject ScreenCanvas;
	public GameObject Panel;

	public GameObject activeRow;
	public string[] solution;
	string[] possibleColors;

	GameObject playerArea;
	GameObject solutionArea;
	GameObject submitArea;
	GameObject mainCamera;

	// Game State
	public int currentAttempt = 0;

	// Use this for initialization
	void Start () {
		mainCamera = (GameObject)Instantiate(MainCamera);
		// TODO: Create UI so player can set these options for themselves
		balls = 8;
		complexity = 4;
		tries = 10;
		repeat = false;

		// Draw the game board
		playerArea = CreatePlayerArea();
		solutionArea = CreateSolutionArea();
		submitArea = CreateSubmitArea();

		// Create the random solution
		possibleColors = new string[8] {"red", "blue", "green", "gray", "cyan", "magenta", "yellow", "black"};
		solution = createSolution(possibleColors);

		Debug.Log (solution[0]);
		Debug.Log (solution[1]);
		Debug.Log (solution[2]);
		Debug.Log (solution[3]);


		// Start the players first attempt
		activeRow = StartAttempt();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Creates the player section filled with the # of balls used in game
	GameObject CreatePlayerArea () {
		// Pass ball number to player area script
		PlayerArea playerScript = PlayerArea.GetComponent<PlayerArea>();
		playerScript.balls = balls;
		PlayerArea.transform.localScale = new Vector3(PlayerArea.transform.localScale.x, PlayerArea.transform.localScale.y, balls + 2);

		// Draw player area
		GameObject playerObject = (GameObject)Instantiate(PlayerArea);
		playerObject.transform.parent = transform;
		return playerObject;
	}

	// Creates the solution area where the player tests his combinations
	GameObject CreateSolutionArea () {
		// Pass solution complexity and tries to code area script
		CodeArea codeScript = CodeArea.GetComponent<CodeArea>();
		codeScript.tries = tries;
		codeScript.complexity = complexity;
		codeScript.repeat = repeat;

		CodeArea.transform.localScale = new Vector3(CodeArea.transform.localScale.x, CodeArea.transform.localScale.y, tries + 9);

		// Draw code area
		GameObject codeObject = (GameObject)Instantiate(CodeArea);
		codeObject.transform.parent = transform;
		return codeObject;
	}

	GameObject CreateSubmitArea () {
		GameObject submitArea = (GameObject)Instantiate(SubmitArea);
		submitArea.transform.parent = transform;

		return submitArea;
	}

	string[] createSolution (string[] colors) {
		// Solution Area
		string[] solution = new string[complexity];
		for (int i=0; i<complexity; i++) {
			// If we've found a new random color or not
			bool foundColor = false;
			// Need to loop as many times as it takes to find a new color
			while (!foundColor) {
				// Find a random member from the possible colors arary
				string randomColor = colors[UnityEngine.Random.Range (0, colors.Length - 1)];
				// If the game is set to allow repeated colors, we're done
				if (repeat) {
					solution[i] = randomColor;
					break;
				}
				// Set to true if we find a repeat color in the solution
				bool repeatColor = false;
				// Loops through the preceding members of the solution array to find possible repeats
				for (int n=i-1; n>-1; n--) {
					// If the random color just found matches the solution, the while loop will repeat.
					if (randomColor == solution[n]) {
						repeatColor = true;
						break;
					}
				}
				// If we haven't found a repeat color, set it to the solution array, break out 
				// of the while loop, and continue creating the rest of the solution
				if (!repeatColor) {
					foundColor = true;
					solution[i] = randomColor;
				}
			}
		}
		return solution;
	}

	public GameObject StartAttempt () {
		// Row that includes both the solution tester and code key
		GameObject row = solutionArea.transform.GetChild (currentAttempt).gameObject;
		SolutionRow rowScript = row.GetComponent<SolutionRow>();
		GameObject[] solutionRow = rowScript.ballDrops;

		for (int i=0; i<complexity; i++) {
			// Adds a collider to each individual part so that the player can start dropping their solution balls
			BoxCollider collider = solutionRow[i].AddComponent<BoxCollider>();
			collider.size = new Vector3 (.5f, 1.4f, 2);
			collider.isTrigger = true;
		}

		//GameObject submitButton = CreateSubmitButton(row, rowScript);
		return row;
	}

	public void LockRow(int rowNum) {
		// Row that includes both the solution tester and code key
		GameObject row = solutionArea.transform.GetChild (rowNum).gameObject;
		SolutionRow rowScript = row.GetComponent<SolutionRow>();
		GameObject[] solutionRow = rowScript.ballDrops;
		
		for (int i=0; i<complexity; i++) {
			// Kills the collider in each row to prevent the player from altering it further
			Destroy(solutionRow[i].GetComponent<BoxCollider>());
		}
	}

	public void ShowSolution () {
		for (int i=0; i<complexity; i++) {
			GameObject solutionBall = solutionArea.GetComponent<CodeArea>().solution[i];
			Color color;
			switch(solution[i]) {
			case "red":
				color = Color.red;
				break;
			case "blue":
				color = Color.blue;
				break;
			case "yellow":
				color = Color.yellow;
				break;
			case "green":
				color = Color.green;
				break;
			case "cyan":
				color = Color.cyan;
				break;
			case "gray":
				color = Color.gray;
				break;
			case "black":
				color = Color.black;
				break;
			case "magenta":
				color = Color.magenta;
				break;
			default:
				color = Color.red;
				break;
			}
			solutionBall.GetComponent<MeshRenderer>().material.color = color;
		}
	}

	public void SubmitAttempt() {
		GameObject[] solutionRow = activeRow.GetComponent<SolutionRow>().ballDrops;

		// Check for incomplete or invalid answers
		if (invalidSubmit(solutionRow)){return;}

		string[] attempt = BuildAttempt (solutionRow);
		String[] results = TestAttempt(attempt);

		RenderResults (results);
		LockRow (currentAttempt);

		if (Array.TrueForAll (results, AreAllMatches)) {
			Debug.Log ("success");
			ShowSolution();
			//ShowSuccess ();
		} else {
			currentAttempt++;
			activeRow = StartAttempt();
		}
	}

	void ShowSuccess() {
		GameObject canvas = (GameObject)Instantiate(ScreenCanvas);
		canvas.GetComponentInParent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		canvas.GetComponentInParent<RectTransform>().sizeDelta = new Vector2(100, 100);
		canvas.name = "Success pane";
		canvas.GetComponentInParent<Transform>().SetParent(transform);

		canvas.GetComponent<MessagePanel>().CreatePanel ();
	}

	bool invalidSubmit(GameObject[] solutionRow) {
		for (int i=0; i<complexity; i++) {
			BallDrop ballScript = solutionRow[i].GetComponent<BallDrop>();
			// If any color is null, return due to invalid submit.
			// TODO: Create error message
			if (ballScript.color == null) {return true;}
		}
		return false;
	}

	static bool AreAllMatches(string value) {
		return (value == "exact");
	}

	string[] BuildAttempt (GameObject[] solutionRow) {
		string[] attempt = new string[complexity];
		// Gathers attempt array to determine answer.
		for (int i=0; i<complexity; i++) {
			BallDrop ballScript = solutionRow[i].GetComponent<BallDrop>();
			attempt[i] = ballScript.color;
		}
		return attempt;
	}
	
	string[] TestAttempt (string[] attempt) {
		string[] results = new string[complexity];
		for (int i=0; i<solution.Length; i++) {
			// if the solution and attempt match on this particular ball
			if (solution[i] == attempt[i]) {
				results[i] = "exact";
			} else {
				// Else, loop through the other attempts to find a non-exact match
				for (int n=0; n<attempt.Length; n++) {
					if (solution[i] == attempt[n]) {
						results[i] = "found";
						break;
						// if the loop is about to end, we haven't found anything
					} else if (n+1 >= attempt.Length) {
						results[i] = "none";
					}
				}
			}
		}
		return results;
	}
	
	void RenderResults (string[] results) {
		// Sort results array
		Array.Sort<string>(results);
		
		GameObject[] keys = activeRow.GetComponent<SolutionRow>().solutionKeys;
		
		Debug.Log (keys[0].name);
		for (int i=0; i<keys.Length; i++) {
			if (results[i] == "exact") {
				keys[i].GetComponent<MeshRenderer>().material.color = Color.red;
			} else if (results[i] == "found") {
				keys[i].GetComponent<MeshRenderer>().material.color = Color.yellow;
			}
		}
	}

	public void RestartGame() {
		currentAttempt = 0;
		Destroy (solutionArea);
		solutionArea = CreateSolutionArea();
		solution = createSolution (possibleColors);
		activeRow = StartAttempt ();

		Debug.Log (solution[0]);
		Debug.Log (solution[1]);
		Debug.Log (solution[2]);
		Debug.Log (solution[3]);
	}
}
