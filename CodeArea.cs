﻿using UnityEngine;
using System.Collections;

public class CodeArea : MonoBehaviour {

	public class Dimensions {
		public float xMin;
		public float xMax;
		public float yMin;
		public float yMax;
		
		public float width;
		public float height;
		
		public Vector2 midpoint;
	}

	public int complexity;
	public int tries;
	public bool repeat;

	public GameObject solutionRow;
	public GameObject ballDrop;
	public GameObject solutionKey;
	public GameObject testKey;

	public GameObject[] solution;

	MeshRenderer mesh;
	Dimensions dimensions;

	// Use this for initialization
	void Awake () {
		transform.position = new Vector3(50, 0, 0);
		transform.rotation = Quaternion.Euler (90, 0, 0);

		// Grab bounds data 
		mesh = GetComponent<MeshRenderer>();
		dimensions = GetDimensions ();

		DrawSolutionRows ();
		DrawSolution ();
	}

	Dimensions GetDimensions () {
		Dimensions dimensions = new Dimensions();
		dimensions.xMin = mesh.bounds.min.x;
		dimensions.xMax = mesh.bounds.max.x;
		dimensions.yMin = mesh.bounds.min.y;
		dimensions.yMax = mesh.bounds.max.y;

		dimensions.width = dimensions.xMax - dimensions.xMin;
		dimensions.height = dimensions.yMax - dimensions.yMin;

		dimensions.midpoint = new Vector2(dimensions.width / 2, dimensions.height / 2);

		return dimensions;
	}

	void DrawSolutionRows(){
		for (int i=1; i<tries + 1; i++) {
			DrawRow(i);
		}
	}

	void DrawRow(int rowNum) {
		float yPos = dimensions.yMin + (15 * rowNum);
		float xPos = dimensions.xMin + 25;

		GameObject row = (GameObject)Instantiate (solutionRow, new Vector3(0, 0, 0), Quaternion.identity);
		row.name = "Game row " + rowNum.ToString ();
		row.transform.parent = transform;

		GameObject test = new GameObject("Test case");
		test.transform.parent = row.transform;

		GameObject[] dropRow = new GameObject[complexity];
		// Draws area where balls are dragged to
		for (int i=0; i<complexity; i++) {
			dropRow[i] = (GameObject)Instantiate (ballDrop, new Vector3(xPos, yPos, -15), Quaternion.identity);
			dropRow[i].transform.parent = test.transform;
			dropRow[i].name = i.ToString ();
			BallDrop ballDropScript = dropRow[i].GetComponent<BallDrop>();
			ballDropScript.gameRow = dropRow;
			ballDropScript.rowIndex = i;
			xPos += 18;
		}

		// Draws solution key area that informs you of your test case's accuracy after submission
		float xStart = xPos;

		// For submit button placement
		float xMax = 0;

		GameObject key = new GameObject("Test Row key");
		key.transform.parent = row.transform;

		// Budges the y pos up slightly so we can align the drop row with the test row
		float testY = yPos + 3;

		GameObject[] testRow = new GameObject[complexity];
		// Separates test keys into two rows for space efficiency
		for (int i=0; i<complexity; i++) {
			int rowLength;
			// If this is the bottom row
			if (complexity%2 != 0) {
				rowLength = (complexity - 1) / 2;
			} else {
				rowLength = complexity / 2;
			}
			testRow[i] = (GameObject)Instantiate(testKey, new Vector3(xPos, testY, -5), Quaternion.identity);
			testRow[i].transform.parent = key.transform;

			if (i%2 == 0) {
				testY -= 6;
			} else {
				testY += 6;
				xPos += 7;
			}
		}

		// Sends a variety of info to the row script for later use
		SolutionRow rowScript = row.GetComponent<SolutionRow>();
		rowScript.ballDrops = dropRow;
		rowScript.solutionKeys = testRow;
		rowScript.xMax = xMax;
		rowScript.yCoord = yPos;
	}

	void DrawSolution(){
		float yCoord = dimensions.yMax - 15;
		float xCoord = dimensions.xMin + 25;
		solution = new GameObject[complexity];
		for (int i=0; i<complexity; i++) {
			solution[i] = (GameObject)Instantiate (solutionKey, new Vector3(xCoord, yCoord, -15), Quaternion.identity);
			solution[i].transform.parent = transform;
			xCoord += 18;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
