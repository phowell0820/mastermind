﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public string color;

	private MeshRenderer render;
	private Vector3 initialPos;
	private Vector3 dropPos;
	private Vector3 screenPoint;
	private Vector3 offset;

	// The game objects MeshRenderer
	MeshRenderer mesh;

	// First ball collider entered (and first one to potentially exit)
	//Collider ballDrop;

	// Second ball collider entered (sicne the ball will enter a second collider before exiting the first)
	//Collider ballDrop2;

	public GameObject ballDrop;
	public bool isDrop = false;

	// Use this for initialization
	void Start () {
		render = GetComponent<MeshRenderer> ();
		initialPos = transform.position;

		mesh = GetComponent<MeshRenderer>();
	}

	void OnMouseDrag() {
		// Point on screen where ball is
		Vector3 currentScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

		// Current position (created by mouse position plus an offest which causes the ball to lag behind)
		Vector3 currentPosition = Camera.main.ScreenToWorldPoint (currentScreenPoint) + offset;

		// Set ball transform
		transform.position = currentPosition;
	}

	public void OnMouseDown() {
		// Finds the distance from the camera to the game object
		screenPoint = Camera.main.WorldToScreenPoint (gameObject.transform.position);

		// Creates an offset based on the mouse position relative to the gameObjects position
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(
			new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z + 5)
		);
	}

	/*
	 * Figures out which collider is more relevant to the balls position and changes the dropBall's material to match
	 * the currently selected player ball.
	 */
	void OnMouseUp() {
		if (ballDrop) {
			ballDrop.GetComponent<MeshRenderer>().material = mesh.material;
			ballDrop.GetComponent<BallDrop>().color = color;
		}
		if (isDrop) {
			Destroy (gameObject);
		} else {
			gameObject.transform.position = initialPos;
		}
	}

	void OnTriggerEnter (Collider other) {
		ballDrop = other.gameObject;
	}

	void OnTriggerExit (Collider other) {
		ballDrop = null;
	}
}
