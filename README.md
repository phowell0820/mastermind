# README #

This is my first entry for the Game of the Month club for September 2015.

The game is a clone of mastermind and uses Unity with a C# codebase.

Users have 10 tries to guess an exact code of 4 different colored balls from a choice of 8 total colors.  The game informs the user after each attempt how accurate their answer was by showing them how many of the balls they used were a.) the correct color and b.) in the correct position.  If the user guesses the exact code of 4 colors, they win.