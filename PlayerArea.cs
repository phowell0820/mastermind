﻿using UnityEngine;
using System.Collections;

public class PlayerArea : MonoBehaviour {

	public class Dimensions {
		public float xMin;
		public float xMax;
		public float yMin;
		public float yMax;
		
		public float width;
		public float height;
		
		public float midpoint;
	}

	public int balls;
	public GameObject ball;

	MeshRenderer mesh;
	Dimensions dimensions;


	// Use this for initialization
	void Awake () {
		transform.position = new Vector3(-140, 0, 0);
		transform.rotation = Quaternion.Euler (90, 0, 0);

		// Grab bounds data
		mesh = GetComponent<MeshRenderer>();
		dimensions = GetDimensions ();

		// Grab row data from number of balls
		int rows;
		if (balls%2==0) {
			rows = balls / 2;
		} else {
			rows = (balls+1) / 2;
		} 

		for (int i=1; i<rows + 1; i++) {
			// Sends iterator for color purposes
			DrawRow (i);
		}
	}

	Dimensions GetDimensions () {
		Dimensions dimensions = new Dimensions();
		dimensions.xMin = mesh.bounds.min.x;
		dimensions.xMax = mesh.bounds.max.x;
		dimensions.yMin = mesh.bounds.min.y;
		dimensions.yMax = mesh.bounds.max.y;
		
		dimensions.width = dimensions.xMax - dimensions.xMin;
		dimensions.height = dimensions.yMax - dimensions.yMin;
		
		return dimensions;
	}

	void DrawRow(int rowNum) {
		float yCoord = dimensions.yMax - ((rowNum - 1) * 12) - (8 * rowNum) - 10;

		Vector3 pos1 = new Vector3(dimensions.xMin + 15, yCoord, -20);
		Vector3 pos2 = new Vector3(dimensions.xMax - 15, yCoord, -20);

		// Get ball color
		Color color1, color2; 
		string sColor1, sColor2;
		switch (rowNum){
		case 1:
			color1 = Color.red;
			color2 = Color.blue;
			sColor1 = "red";
			sColor2 = "blue";
			break;
		case 2:
			color1 = Color.yellow;
			color2 = Color.green;
			sColor1 = "yellow";
			sColor2 = "green";
			break;
		case 3: 
			color1 = Color.cyan;
			color2 = Color.gray;
			sColor1 = "cyan";
			sColor2 = "gray";
			break;
		case 4:
			color1 = Color.black;
			color2 = Color.magenta;
			sColor1 = "black";
			sColor2 = "magenta";
			break;
		default:
			color1 = Color.red;
			color2 = Color.blue;
			sColor1 = "red";
			sColor2 = "blue";
			break;
		}

		GameObject ball1 = (GameObject)Instantiate (ball, pos1, Quaternion.identity);
		GameObject ball2 = (GameObject)Instantiate (ball, pos2, Quaternion.identity);

		ball1.transform.parent = transform;
		ball2.transform.parent = transform;

		ball1.GetComponent<Ball>().color = sColor1;
		ball2.GetComponent<Ball>().color = sColor2;

		ball1.GetComponent<MeshRenderer>().material.color = color1;
		ball2.GetComponent<MeshRenderer>().material.color = color2;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
