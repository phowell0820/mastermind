﻿using UnityEngine;
using System.Collections;

public class BallDrop : MonoBehaviour {

	public string color;

	public GameObject dragDrop;
	public Material shader;

	// Caches the row object that this ball drop belongs to along with its index in the row
	public GameObject[] gameRow;
	public int rowIndex;

	MeshRenderer mesh;
	GameObject drag;

	// Use this for initialization
	void Start () {
		mesh = GetComponent<MeshRenderer>();
		mesh.material.color = new Color(.3f, .3f, .3f);
		color = null;
	}

	void OnMouseDown () {
		if (color != null) {
			if (gameObject.GetComponent<Ball>() == null) {
				Ball ballScript = gameObject.AddComponent<Ball>();
				ballScript.color = color;
				ballScript.isDrop = true;
			}
			drag = (GameObject)Instantiate (dragDrop, transform.position, Quaternion.identity);
			drag.GetComponent<MeshRenderer>().materials[0] = shader;
			drag.transform.parent = transform.parent;
			gameRow[rowIndex] = drag;
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
