﻿using UnityEngine;
using System.Collections;

public class SolutionKey : MonoBehaviour {

	MeshRenderer mesh;

	// Use this for initialization
	void Start () {
		mesh = GetComponent<MeshRenderer>();
		mesh.material.color = new Color(.3f, .3f, .3f); 	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
