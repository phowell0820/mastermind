﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class SubmitArea : MonoBehaviour {

	public class Dimensions {
		public float xMin;
		public float xMax;
		public float yMin;
		public float yMax;
		
		public float width;
		public float height;
		
		public Vector2 midpoint;
	}

	// Prefab
	public GameObject buttonPrefab;

	GameObject screenCanvas;

	static GameObject gameManager;	
	static GameManager gameManagerScript;

	// Use this for initialization
	void Start () {
		transform.position = new Vector3 (-140, -77, 0);
		transform.rotation = Quaternion.Euler (90, 0, 0);
		gameManager = transform.parent.gameObject;
		gameManagerScript = gameManager.GetComponent<GameManager>();

		RenderCanvas();
	}

	void RenderCanvas () {
		screenCanvas = (GameObject)Instantiate(gameManagerScript.ScreenCanvas);
		screenCanvas.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
		screenCanvas.name = "InGame UI";
		screenCanvas.transform.SetParent(transform);

		GameObject submitButton = RenderSubmit ();
		GameObject restartButton = RenderRestart ();
	}

	GameObject RenderSubmit() {
		GameObject submitButton = (GameObject)Instantiate (buttonPrefab);
		submitButton.transform.SetParent (screenCanvas.transform, false);
		submitButton.transform.position = Vector3.zero;
		submitButton.name = "Submit";

		RectTransform button = submitButton.GetComponent<RectTransform>();
		button.localScale = new Vector3(.3f, .3f, .3f);
		button.sizeDelta = new Vector2(120, 30);
		button.position = new Vector3(-140, -70);

		Button buttonScript = submitButton.GetComponent<Button>();
		buttonScript.onClick.AddListener (() => gameManagerScript.SubmitAttempt ());

		submitButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = "Submit";

		return submitButton;
	}

	GameObject RenderRestart () {
		GameObject restartButton = (GameObject)Instantiate (buttonPrefab);
		restartButton.transform.SetParent (screenCanvas.transform, false);
		restartButton.transform.position = Vector3.zero;
		restartButton.name = "Restart";

		RectTransform button = restartButton.GetComponent<RectTransform>();
		button.localScale = new Vector3(.3f, .3f, .3f);
		button.sizeDelta = new Vector2(120, 30);
		button.position = new Vector3(-140, -85);

		Button buttonScript = restartButton.GetComponent<Button>();
		buttonScript.onClick.AddListener (() => gameManagerScript.RestartGame());

		restartButton.transform.GetChild (0).gameObject.GetComponent<Text>().text = "Restart";
	
		return restartButton;
	}

	// Update is called once per frame
	void Update () {
	
	}

	void SubmitAnswer () {

	}

	public void OnSubmit() {
		gameManagerScript.SubmitAttempt();
	}

	public void OnRestart () {
		gameManagerScript.RestartGame ();
	}
}
